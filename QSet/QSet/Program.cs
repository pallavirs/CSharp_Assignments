﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Diagnostics.Stopwatch;
using System.Diagnostics;

namespace QSet
{
    public class Program
    {
        static void Main(string[] args)
        {
       
            Stopwatch sw = Stopwatch.StartNew();
            Questions q = new Questions();
            q.PrintQue();
            sw.Stop();
            Console.WriteLine("Time taken: {0}ms", sw.Elapsed.TotalMilliseconds);
            Console.ReadLine();

        }
    }
}
