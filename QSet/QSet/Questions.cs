﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QSet
{
    public class Questions
    {
        List<string> A = new List<string>();
        List<string> B = new List<string>();
        List<string> C = new List<string>();
        List<string> D = new List<string>();

        List<string> QuestionPaper = new List<string>();
        public void GenerateQue()
        {
            for (int i = 0; i < 1000; i++)
            {
                A.Add("A" + i);
            }
            for (int i = 0; i < 5000; i++)
            {
                B.Add("B" + i);
            }
            for (int i = 0; i < 6000; i++)
            {
                C.Add("C" + i);
            }
            for (int i = 0; i < 2000; i++)
            {
                D.Add("D" + i);
            }
        }
        public int FindMax(int[] arr)
        {
            int index = 0;
            int max = -1;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    index = i;
                }
            }
            return index;

        }
        public int FindSecondMax(int[] arr)
        {
            int maxindex = FindMax(arr);
            int max = -1;
            int index = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (maxindex == i)
                    continue;

                else if(arr[i]> max)
                {
                    max = arr[i];
                    index = i;
                }
            }
            return index;
        }
        public void PrintQue()
        {
            int count1 = 0, count2 = 0, count3 = 0, count4 = 0;
            int aindex = 0, bindex = 0, cindex = 0, dindex = 0;
            GenerateQue();
            count1 = A.Count;
            count2 = B.Count;
            count3 = C.Count;
            count4 = D.Count;
            int tot_count = count1 + count2 + count3 + count4;
            int[] countArray = new int[4] { count1, count2, count3, count4 };
            int preIndex = 0;
            

            for (int i = 0; i < tot_count; i++)
            {
                int index = FindMax(countArray);

                if (preIndex == index)
                    index = FindSecondMax(countArray);


                countArray[index]--;

                if (index == 0)
                {
                    //Console.WriteLine(" " + A[aindex]);
                    QuestionPaper.Add(A[aindex]);
                    aindex++;
                }
                else if (index == 1)
                {

                    //Console.WriteLine(" " + B[bindex]);
                    QuestionPaper.Add(B[bindex]);
                    bindex++;
                }
                else if (index == 2)
                {
                    //Console.WriteLine(" " + C[cindex]);
                    QuestionPaper.Add(C[cindex]);
                    cindex++;
                }
                else if (index == 3)
                {
                    //Console.WriteLine(" " + D[dindex]);
                    QuestionPaper.Add(D[dindex]);
                    dindex++;
                }
                preIndex = index;
            }


            foreach (string s in QuestionPaper)
            {
                Console.WriteLine(s);
            }
            //Console.ReadLine();
        }

        }
 }

